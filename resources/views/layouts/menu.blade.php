<span style="float: right;">
    <a href="{{route('author.book.index')}}" style="color: blue;">
        {{ __('Show All Books') }}
    </a>
</span>
<span style="float: right;color: black;margin: 0px 10px;">{{ ' - ' }}</span>
<span style="float: right;">
    <a href="{{route('author.book.add')}}" style="color: green;">
        {{ __('Add Book') }}
    </a>
</span>