@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span class="col-md-12" style="float: right;">
                            {{ 'Show Book Detail' }}
                        </span>
                        @if(\App\Models\Basic\User::getById(Illuminate\Support\Facades\Auth::id())->getJob() == 'author')
                            @include('layouts.menu')
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <div class="col-md-3" style="float: right;">{{'Author'}}</div>
                        <div class="col-md-3" style="float: right;">{{'title'}}</div>
                        <div class="col-md-2" style="float: right;">{{'created At'}}</div>
                        <div class="col-md-2" style="float: right;">{{''}}</div>
                        <div class="col-md-2" style="float: right;">{{''}}</div>
                            <hr>
                        @if($book)
                            <div class="col-md-3" style="float: right;">{{\App\Models\Basic\User::getById($book['user_id'])->getFullName()}}</div>
                            <div class="col-md-3" style="float: right;">{{$book['title']}}</div>
                            <div class="col-md-2" style="float: right;">{{$book['created_at']}}</div>
                            <div class="col-md-2" style="float: right;">
                                <a class="btn btn-primary" href="{{route('author.book.edit',[\App\Models\Basic\Book::COLUMN_ID=>$book['id']])}}">
                                    {{'EDIT'}}
                                </a>
                            </div>
                            <div class="col-md-2" style="float: right;">
                                <a class="btn btn-danger" href="{{route('author.book.del',[\App\Models\Basic\Book::COLUMN_ID=>$book['id']])}}">
                                    {{'Delete'}}
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
