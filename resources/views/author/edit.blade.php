@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span class="col-md-12" style="float: right;">
                            {{ 'Edit Book' }}
                        </span>
                        @if(\App\Models\Basic\User::getById(Illuminate\Support\Facades\Auth::id())->getJob() == 'author')
                            @include('layouts.menu')
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                            <div class="col-md-3" style="float: right;">{{'Author'}}</div>
                            <div class="col-md-3" style="float: right;">{{'title'}}</div>
                            <div class="col-md-2" style="float: right;">{{'created At'}}</div>
                            <div class="col-md-2" style="float: right;">{{''}}</div>
                            <hr>
                            @if($book)
                                <form method="post" action="{{route('author.book.save',[\App\Models\Basic\Book::COLUMN_ID=>$book['id']])}}">
                                    <div class="col-md-3" style="float: right;">{{\App\Models\Basic\User::getById($book['user_id'])->getFullName()}}</div>
                                    <div class="col-md-3" style="float: right;">
                                        <input class="form-control" type="text" name="title" value="{{$book['title']}}" style="text-align: right;">
                                    </div>
                                    <div class="col-md-2" style="float: right;">{{$book['created_at']}}</div>
                                    <div class="col-md-2" style="float: right;">
                                        <button class="btn btn-primary">{{'ثبت کتاب'}}</button>
                                    </div>
                                    @csrf
                                </form>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
