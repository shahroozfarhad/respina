@extends('layouts.app')
@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">
                        <span class="col-md-12" style="float: right;">
                            {{ 'Add Book' }}
                        </span>
                        @if(\App\Models\Basic\User::getById(Illuminate\Support\Facades\Auth::id())->getJob() == 'author')
                            @include('layouts.menu')
                        @endif
                    </div>

                    <div class="card-body">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="post" action="{{route('author.book.save')}}">
                            <input class="form-control" style="text-align: right;" type="text" name="title" placeholder="{{'عنوان کتاب'}}" required>
                            <br>
                            <button class="btn btn-primary">{{'ثبت کتاب'}}</button>
                            @csrf
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
