<?php

use App\Database\Migrations\BaseMigration;
use App\Models\Basic\Book;
use Illuminate\Database\Schema\Blueprint;

class CreateBooksTable extends BaseMigration
{
    public function __construct()
    {
        parent::__construct(Book::getDBTable());
    }

    protected function createTable(Blueprint $table)
    {
        $table->integer(Book::COLUMN_USER_ID, false,true)
            ->nullable(true);
        $table->string(Book::COLUMN_TITLE, 255)
            ->nullable(true);

        $index = 'i_'.Book::getDBTable().'_'.Book::COLUMN_TITLE;
        $table->index(
            [
                Book::COLUMN_TITLE,
            ],
            $index
        );
    }

    protected function alterTable(Blueprint $table)
    {
        if (!$this->hasColumn(Book::COLUMN_USER_ID)) {
            $table->integer(Book::COLUMN_USER_ID, false,true)
                ->nullable(false)
                ->after(Book::COLUMN_ID);
        }
        if (!$this->hasColumn(Book::COLUMN_TITLE)) {
            $table->string(Book::COLUMN_TITLE, 255)
                ->nullable(false)
                ->after(Book::COLUMN_USER_ID);
        }

        $index = 'i_'.Book::getDBTable().'_'.Book::COLUMN_TITLE;
        if ($this->hasIndex($index)) {
            $table->dropUnique($index);
        }
        if (!$this->hasIndex($index)) {
            $table->unique(
                [
                    Book::COLUMN_TITLE,
                ],
                $index
            );
        }
    }
}
