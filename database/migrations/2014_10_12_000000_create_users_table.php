<?php

use App\Database\Migrations\BaseMigration;
use App\Models\Basic\User;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends BaseMigration
{
    public function __construct()
    {
        parent::__construct(User::getDBTable());
    }

    protected function createTable(Blueprint $table)
    {
        $table->string(User::COLUMN_FIRST_NAME, 255)
            ->nullable(true);
        $table->string(User::COLUMN_LAST_NAME, 255)
            ->nullable(true);
        $table->string(User::COLUMN_MOBILE, 255)
            ->nullable(true);
        $table->string(User::COLUMN_EMAIL, 255)
            ->nullable(true);
        $table->string(User::COLUMN_JOB, 255)
            ->nullable(true);
        $table->string(User::COLUMN_PASSWORD, 255)
            ->nullable(true);
        $table->rememberToken();

        $index = 'u_'.User::getDBTable().'_'.User::COLUMN_EMAIL;
        $table->unique(
            [
                User::COLUMN_EMAIL,
            ],
            $index
        );
    }

    protected function alterTable(Blueprint $table)
    {
        if (!$this->hasColumn(User::COLUMN_FIRST_NAME)) {
            $table->string(User::COLUMN_FIRST_NAME, 255)
                ->nullable(true)
                ->after(User::COLUMN_ID);
        }
        if (!$this->hasColumn(User::COLUMN_LAST_NAME)) {
            $table->string(User::COLUMN_LAST_NAME, 255)
                ->nullable(true)
                ->after(User::COLUMN_FIRST_NAME);
        }
        if (!$this->hasColumn(User::COLUMN_MOBILE)) {
            $table->string(User::COLUMN_MOBILE, 255)
                ->nullable(true)
                ->after(User::COLUMN_LAST_NAME);
        }
        if (!$this->hasColumn(User::COLUMN_EMAIL)) {
            $table->string(User::COLUMN_EMAIL, 255)
                ->nullable(true)
                ->after(User::COLUMN_MOBILE);
        }
        if (!$this->hasColumn(User::COLUMN_JOB)) {
            $table->string(User::COLUMN_JOB, 255)
                ->nullable(true)
                ->after(User::COLUMN_EMAIL);
        }
        if (!$this->hasColumn(User::COLUMN_PASSWORD)) {
            $table->string(User::COLUMN_PASSWORD, 255)
                ->nullable(true)
                ->after(User::COLUMN_EMAIL);
        }
        if (!$this->hasColumn(User::COLUMN_REMEMBER_TOKEN)) {
            $table->rememberToken();
        }

        $index = 'u_'.User::getDBTable().'_'.User::COLUMN_EMAIL;
        if ($this->hasIndex($index)) {
            $table->dropUnique($index);
        }
    }
}
