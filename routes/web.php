<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome',['title'=>'صفحه اصلی']);
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(
    [
        'middleware' => 'auth'
    ],
    function (){

        Route::get('author/book/index', 'AuthorController@index')->name('author.book.index');

        Route::get('author/book/show', 'AuthorController@show')->name('author.book.show');

        Route::get('author/book/add', 'AuthorController@add')->name('author.book.add');

        Route::post('author/book/save', 'AuthorController@save')->name('author.book.save');

        Route::get('author/book/edit', 'AuthorController@edit')->name('author.book.edit');

        Route::get('author/book/del', 'AuthorController@deleteBook')->name('author.book.del');

    });