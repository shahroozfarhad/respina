<?php
namespace App\Database\Migrations;

use App\Models\BaseModel;
use Doctrine\DBAL\Schema\SchemaException;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Schema;

class BaseMigration extends Migration
{
    private $table;
    private $doctrineTable;
    private $hasBigInt;

    public function __construct(string $table, bool $hasBigInt = false)
    {
        $this->table = $table;
        $this->hasBigInt = $hasBigInt;

        $connection = Schema::getConnection();
        $schemaManager = $connection->getDoctrineSchemaManager();
        $schemaManager->getDatabasePlatform()->registerDoctrineTypeMapping('enum', 'string');
        $this->doctrineTable = $schemaManager->listTableDetails($table);
    }

    /**
     * Run the migrations.
     * @return void
     */
    public function up()
    {
        if (Schema::hasTable($this->table)) {
            Schema::table($this->table, function (Blueprint $table) {
                $this->alterTable($table);
            });
        } else {
            Schema::create($this->table, function (Blueprint $table) {
                $this->appendIdColumn($table);
                $this->createTable($table);
                $this->appendUserIdentifierColumns($table);
                $this->appendTimestampsColumn($table);
                $table->boolean(BaseModel::COLUMN_ACTIVE)
                    ->default(true)
                    ->nullable(false);
                $table->softDeletes();
            });
        }
    }

    private function appendIdColumn(Blueprint $table)
    {
        if ($this->hasBigInt) {
            $table->bigIncrements(BaseModel::COLUMN_ID);
        } else {
            $table->increments(BaseModel::COLUMN_ID);
        }

    }

    private function appendUserIdentifierColumns(Blueprint $table)
    {
        $table->integer(baseModel::COLUMN_CREATED_BY, false, true)
            ->nullable(true);
        $table->integer(baseModel::COLUMN_UPDATED_BY, false, true)
            ->nullable(true);
    }

    private function appendTimestampsColumn(Blueprint $table)
    {
        $table->timestamps();
    }

    protected function createTable(Blueprint $table){}

    protected function alterTable(Blueprint $table){}

    protected function hasIndex(string $indexName): bool
    {
        return $this->doctrineTable->hasIndex($indexName);
    }

    protected function hasPrimaryKey(): bool
    {
        return $this->doctrineTable->hasPrimaryKey();
    }

    protected function hasColumn(string $column): bool
    {
        return $this->doctrineTable->hasColumn($column);
    }

    /**
     * @param string $column
     * @return string|null
     * @throws SchemaException
     */
    protected function getColumnType(string $column): ?string
    {
        try {
            return ($this->doctrineTable->hasColumn($column)) ?
                $this->doctrineTable->getColumn($column)->getType()->getName() :
                null;
        } catch (SchemaException $schemaException) {
            throw $schemaException;
        }
    }

    /**
     * @param string $column
     * @param string $type
     * @return bool
     * @throws SchemaException
     */
    protected function hasType(string $column, string $type): bool
    {
        if($this->doctrineTable->hasColumn($column)) {
            try {
                return ($this->doctrineTable->getColumn($column)->getType()->getName() == $type);
            } catch (SchemaException $schemaException) {
                throw $schemaException;
            }
        } else {
            return false;
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists($this->table);
    }
}
