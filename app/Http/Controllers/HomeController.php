<?php

namespace App\Http\Controllers;

use Auth;

class HomeController extends BaseController
{

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        return $this->returnView('home', []);
    }
}
