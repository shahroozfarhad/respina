<?php
namespace App\Http\Controllers;

use App\Models\Basic\Book;
use Illuminate\Support\Facades\Auth;

class AuthorController extends BaseController
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * @return Book Collection
     */
    public function index()
    {
        $books = null;

        if(Auth::check()){
            // get all books by Auth id
            $books = Book::getAllByUserId(Auth::id());
        }

        return $this->returnView('author.index', [
            'books' => $books,
        ],
        'لیست کتاب ها',
        'کتابخانه جامع'
        );
    }

    /**
     * @return Book detail
     */
    public function show()
    {
        $book_id = request(Book::COLUMN_ID);
        // get book data by book id
        $book = Book::getById($book_id);

        return $this->returnView('author.show', [
            'book' => $book,
        ],
            'نمایش کتاب',
            'کتابخانه جامع'
            );
    }

    /**
     * @get Book data
     */
    public function add()
    {
        return $this->returnView('author.add', [],
            'ایجاد کتاب',
            'کتابخانه جامع'
            );
    }

    /**
     * @param Book COLUMN_ID
     * @param Book COLUMN_TITLE
     *
     * @return Book data
     */
    public function edit()
    {
        $book_id = request(Book::COLUMN_ID);
        // get book data by book id
        $book = Book::getById($book_id);

        return $this->returnView('author.edit', [
            'book' => $book
        ],
            'ویرایش کتاب',
            'کتابخانه جامع'
            );
    }

    /**
     * @param Book COLUMN_ID
     * @param Book COLUMN_TITLE
     *
     * @return Book save
     */
    public function save()
    {
        if(Auth::check()){
            $user_id = Auth::id();
        }

        $book_id = request(Book::COLUMN_ID);
        $title = request(Book::COLUMN_TITLE);

        // check has book by $book_id
        // if has id -> book updated
        // else book add to book table
        if($book_id){
            $book = (new Book)
                ->where(Book::COLUMN_ID,'=',$book_id)
                ->first();
            $book
                ->setTitle($title)
                ->save();
        } else {
            $book = (new Book);
            $book
                ->setUserId($user_id)
                ->setTitle($title)
                ->save();
        }

        return redirect()->to('author/book/index');
    }

    /***
     * delete book row by Book COLUMN_ID
     */
    public function deleteBook()
    {
        $book_id = request(Book::COLUMN_ID);

        if($book_id){
            // delete book data by book id
            Book::getById($book_id)->delete();
        }

        return redirect()->to('author/book/index');
    }
}