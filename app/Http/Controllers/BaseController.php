<?php

namespace App\Http\Controllers;

class BaseController extends Controller
{
    const TITLE        = 'کتاب';
    const DESCRIPTION  = 'بانک اطلاعاتی کتاب';

    public function returnView(string $view, array $data = [], ?string $title = self::TITLE, ?string $description = self::DESCRIPTION)
    {

        return view(
            $view,
            array_merge(
                [
                    'title'       =>$title,
                    'description' =>$description,
                ],
                $data
            )
        );
    }

}
