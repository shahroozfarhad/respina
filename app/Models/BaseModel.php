<?php
namespace App\Models;

use App\Models\Basic\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Facades\Cache;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Log;

/**
 * @mixin Builder;
 * @mixin QueryBuilder;
 */
abstract class BaseModel extends Model implements IBaseModel
{
    use SoftDeletes;
    const CACHE_TTL = 86400;

    const GROUP = 'Basic';

    const COLUMN_ID = 'id';
    const COLUMN_CREATED_BY = 'created_by';
    const COLUMN_CREATED_AT = 'created_at';
    const COLUMN_UPDATED_BY = 'updated_by';
    const COLUMN_UPDATED_AT = 'updated_at';
    const COLUMN_DELETED_AT = 'deleted_at';
    const COLUMN_ACTIVE = 'active';

    const ALL_COLUMNS = '.*';

    private $deletingFiles = [];

    public function __construct(array $attributes = [])
    {
        $this->connection = $this->getDBConnection();
        $this->table = $this->getDBTable();
        parent::__construct($attributes);
    }

    public static function getDBConnection(): string
    {
        return 'mysql';
    }

    abstract public static function getDBTable(): string;

    abstract public static function getGroup(): string;

    public function getId(): int
    {
        return $this->{self::COLUMN_ID};
    }

    public function setId(int $value): self
    {
        $this->{self::COLUMN_ID} = $value;
        return $this;
    }

    public function hasId(): bool
    {
        return $this->{self::COLUMN_ID} !== null;
    }

    public function getCreatedById(): ?int
    {
        return $this->{self::COLUMN_CREATED_BY};
    }

    public function setCreatedById(?int $value): self
    {
        $this->{self::COLUMN_CREATED_BY} = $value;
        return $this;
    }
    public function getCreatedBy(): ?User
    {
        if (!$this->getCreatedById()) {
            return null;
        }

        $user = User::getById($this->getCreatedById());
        $user->name = ($user->getFirstName() || $user->getLastName()) ?
            $user->getFirstName() . ' ' . $user->getLastName() :
            $user->getEmail();

        return $user;
    }

    public function setCreatedByNull(): self
    {
        $this->{self::COLUMN_CREATED_BY} = null;
        return $this;
    }

    public function getUpdatedById(): ?int
    {
        return $this->{self::COLUMN_UPDATED_BY};
    }

    public function setUpdatedById(?int $value): self
    {
        $this->{self::COLUMN_UPDATED_BY} = $value;
        return $this;
    }

    public function getCreatedAt(): ?Carbon
    {
        if ($this->{self::COLUMN_CREATED_AT} === null) {
            return null;
        }
        return Carbon::parse($this->{self::COLUMN_CREATED_AT});
    }

    public function getUpdatedAt(): ?Carbon
    {
        if ($this->{self::COLUMN_UPDATED_AT} === null) {
            return null;
        }
        return Carbon::parse($this->{self::COLUMN_UPDATED_AT});
    }

    public function getIsActive(): bool
    {
        return $this->{self::COLUMN_ACTIVE};
    }

    public function setIsActive(bool $value): self
    {
        $this->{self::COLUMN_ACTIVE} = $value;
        return $this;
    }

    public function isDeleted(): bool
    {
        return $this->{self::COLUMN_DELETED_AT}? true: false;
    }

    public static function getColumns(string $col = '*', string $alias = '', $with_table = true)
    {
        if ($col == '*') {
            $alias = '';
        }
        return ($with_table ? static::getDBTable() . '.' : '') . $col . ($alias ? ' AS ' . $alias : '');
    }

    private static function getEntityItemCacheKey(int $id = 0): string
    {
        return static::getCacheKeyPrefix().':'.$id;
    }

    public static function getCacheKeyPrefix(): string
    {
        return static::getDBConnection().':'.static::getDBTable();
    }

    /**
     * @param int $id
     * @return $this
     */
    public static function getById(int $id)
    {
        $cacheKey = self::getEntityItemCacheKey($id);
        $model = Cache::remember($cacheKey, self::CACHE_TTL, function () use ($id) {
            $modelName = get_called_class();
            /** @var BaseModel $modelObject */
            $modelObject = (new $modelName);
            $model = $modelObject::withTrashed()->find($id);

            if ($model !== null) {
                return $model;
            }
            return null;
        });
        return $model;
    }

    public function getChanges(): array
    {
        return parent::getChanges();
    }

    public function save(array $options = [])
    {
        $this->updateUserFields();
        $result = parent::save($options);

//        if ($result) {
//            $this->deleteFiles();
//            Log::info('SaveLog:',['save'=>$this]);
//            $this->log();
//            $this->refreshCache();
//        }
        return $result;
    }

    /**
     * @return bool|null
     * @throws Exception
     */
    public function delete(): ?bool
    {
        $result = parent::delete();
//        if ($result) {
//            $this->log();
//            $this->forgeIdCache();
//        }
        return $result;
    }

    private function updateUserFields()
    {
        if (Auth::check()) {
            if (!$this->hasId()) {
                $this->setCreatedById(Auth::id());
            }

            $this->setUpdatedById(Auth::id());
        }
    }

    private function forgeIdCache()
    {
        // clear cache
        if ($this->hasId()) {
            $cacheKey = $this->getEntityItemCacheKey($this->getId());
            Cache::forget($cacheKey);
        }
    }

    private function refreshCache()
    {
        if ($this->hasId()) {
            $cacheKey = $this->getEntityItemCacheKey($this->getId());
            Cache::put($cacheKey, $this, self::CACHE_TTL);
        }
    }

    public function addDeletingFile(string $path)
    {
        $this->deletingFiles[] = $path;
    }
}