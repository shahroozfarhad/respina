<?php
namespace App\Models;

interface IBaseModel
{
    static function getDBConnection(): string;
    static function getDBTable(): string;

    function getChanges(): array;

    function hasId(): bool;
    function getId(): int;

    static function getById(int $id);
    function delete(): ?bool;

    function addDeletingFile(string $path);
}