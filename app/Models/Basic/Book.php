<?php
namespace App\Models\Basic;

use App\Models\BaseModel;
use Illuminate\Support\Collection;

class Book extends BaseModel
{
    public static function getDBTable(): string
    {
        return 'book';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_USER_ID  = 'user_id';
    const COLUMN_TITLE    = 'title';


    public function getUserId(): int
    {
        return $this->{self::COLUMN_USER_ID};
    }
    public function setUserId(int $value): self
    {
        $this->{self::COLUMN_USER_ID} = $value;
        return $this;
    }

    public function getTitle(): string
    {
        return $this->{self::COLUMN_TITLE};
    }
    public function setTitle(string $value): self
    {
        $this->{self::COLUMN_TITLE} = $value;
        return $this;
    }

    public static function getById(int $id): self
    {
        return (new Book())
            ->where(Book::COLUMN_ID,'=',$id)
            ->first();
    }

    public static function getAllByUserId(int $userId): Collection
    {
        return (new Book())
            ->where(Book::COLUMN_USER_ID,'=',$userId)
            ->orderBy(Book::COLUMN_ID)
            ->get();
    }

    public static function getAll(int $userId): Collection
    {
        return (new Book())
            ->where(Book::COLUMN_USER_ID,'=',$userId)
            ->orderBy(Book::COLUMN_ID)
            ->get();
    }
}
