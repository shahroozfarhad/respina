<?php
namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Support\Collection;
use App\Models\BaseModel;

/**
 * @mixin Builder;
 * @mixin QueryBuilder;
 */
class UserReferral extends BaseModel
{
    public static function getDBTable(): string
    {
        return 'user_referrals';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_ID                 = 'id';
    const COLUMN_SENDER_USER_ID     = 'sender_user_id';
    const COLUMN_RECEIVER_USER_ID   = 'receiver_user_id';
    const COLUMN_ADV_ID             = 'adv_id';
    const COLUMN_ADV_TITLE          = 'adv_title';
    const COLUMN_DESCRIPTION        = 'description';

    public function getSenderUserId(): ?int
    {
        return $this->{self::COLUMN_SENDER_USER_ID};
    }
    public function setSenderUserId(?int $value): self
    {
        $this->{self::COLUMN_SENDER_USER_ID} = $value;
        return $this;
    }

    public function getReceiverUserId(): ?int
    {
        return $this->{self::COLUMN_RECEIVER_USER_ID};
    }
    public function setReceiverUserId(?int $value): self
    {
        $this->{self::COLUMN_RECEIVER_USER_ID} = $value;
        return $this;
    }

    public function getAdvId(): ?int
    {
        return $this->{self::COLUMN_ADV_ID};
    }
    public function setAdvId(?int $value): self
    {
        $this->{self::COLUMN_ADV_ID} = $value;
        return $this;
    }

    public function getTitle(): ?string
    {
        return $this->{self::COLUMN_ADV_TITLE};
    }
    public function setTitle(?string $value): self
    {
        $this->{self::COLUMN_ADV_TITLE} = $value;
        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->{self::COLUMN_DESCRIPTION};
    }
    public function setDescription(?string $value): self
    {
        $this->{self::COLUMN_DESCRIPTION} = $value;
        return $this;
    }

    public static function getById(int $id): ?self
    {
        return (new UserReferral())
            ->where(UserReferral::COLUMN_ID,'=',$id)
            ->first();
    }

    public static function getBySenderUserId(int $SenderUserid): Collection
    {
        return (new UserReferral())
            ->where(UserReferral::COLUMN_SENDER_USER_ID,'=',$SenderUserid)
            ->orderByDesc(UserReferral::COLUMN_ID)
            ->get();
    }

    public static function getByReceiverUserId(int $ReceiverUserid): Collection
    {
        return (new UserReferral())
            ->where(UserReferral::COLUMN_RECEIVER_USER_ID,'=',$ReceiverUserid)
            ->get();
    }

    public function save(array $options = []): bool
    {
        $result = parent::save($options);

        if ($result && empty($options)) {
            //UserSapIntegrationJob::dispatch($this);
        }

        return $result;
    }
}
