<?php
namespace App\Models\Basic;

use App\Models\BaseModel;

class UserGroups extends BaseModel
{

    public static function getDBTable(): string
    {
        return 'user_groups';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_TYPE   = 'type';
    const COLUMN_NAME   = 'name';
    const COLUMN_ACCESS = 'access';

    const TYPE_SUPER_USER      = 'super_user';
    const TYPE_RANGE_MANAGER   = 'range_manager';
    const TYPE_OFFICE_MANAGER  = 'office_manager';
    const TYPE_CONSULTANT      = 'consultant';
    const TYPE_ASSISTANT       = 'assistant';
    const TYPE_GENERAL         = 'general';

    const TYPES = [
        self::TYPE_SUPER_USER      => 'مدیر ارشد',
        self::TYPE_RANGE_MANAGER   => 'مدیر رنج',
        self::TYPE_OFFICE_MANAGER  => 'مدیر دفتر',
        self::TYPE_CONSULTANT      => 'مشاور',
        self::TYPE_ASSISTANT       => 'منشی',
        self::TYPE_GENERAL         => 'عمومی',
    ];

    const ACCESS_USERS         = 'users';
    const ACCESS_USER_GROUPS   = 'user_groups';
    const ACCESS_FILES         = 'files';
    const ACCESS_TICKETS       = 'tickets';
    const ACCESS_SCHEDULE      = 'schedules';
    const ACCESS_OFF_TIMES     = 'off_times';
    const ACCESS_ARTICLES      = 'articles';
    const ACCESS_GALLERY       = 'gallery';
    const ACCESS_FILTER        = 'filter';

    const ACCESSES = [
        self::ACCESS_USERS        => 'کاربرها',
        self::ACCESS_USER_GROUPS  => 'گروه های کاربری',
        self::ACCESS_FILES        => 'فایلها',
        self::ACCESS_TICKETS      => 'پیامها',
        self::ACCESS_SCHEDULE     => 'جدول زمانبندی',
        self::ACCESS_OFF_TIMES    => 'مرخصی',
        self::ACCESS_ARTICLES     => 'اخبار و مقالات',
        self::ACCESS_GALLERY      => 'گالری عکس',
        self::ACCESS_FILTER       => 'فیلتر',
    ];

    public function getType(): ?string
    {
        return $this->{self::COLUMN_TYPE};
    }
    public function setType(?string $value): self
    {
        $this->{self::COLUMN_TYPE} = $value;
        return $this;
    }

    public function getName(): ?string
    {
        return $this->{self::COLUMN_NAME};
    }
    public function setName(?string $value): self
    {
        $this->{self::COLUMN_NAME} = $value;
        return $this;
    }

    public function getAccess(): ?string
    {
        return json_decode($this->{self::COLUMN_ACCESS},true);
    }

    public function setAccess(string $value): self
    {
        $this->{self::COLUMN_ACCESS} = json_encode($value);
        return $this;
    }
}
