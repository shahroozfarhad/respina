<?php
namespace App\Models\Basic;

use Auth;
use Hash;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Collection;

/**
 * @mixin Builder;
 * @mixin QueryBuilder;
 */
class User extends Authenticatable
{
    public static function getDBTable(): string
    {
        return 'users';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_ID = 'id';
    const COLUMN_FIRST_NAME = 'first_name';
    const COLUMN_LAST_NAME = 'last_name';
    const COLUMN_MOBILE = 'mobile';
    const COLUMN_EMAIL = 'email';
    const COLUMN_JOB = 'job';
    const COLUMN_EMAIL_VERIFIED_AT = 'email_verified_at';
    const COLUMN_MOBILE_VERIFIED_AT = 'mobile_verified_at';
    const COLUMN_PASSWORD = 'password';
    const COLUMN_REMEMBER_TOKEN = 'remember_token';
    const COLUMN_ACTIVE = 'active';


    const JOB_ADMIN   = 'admin';
    const JOB_GENERAL = 'general';
    const JOB_AUTHOR  = 'author';

    const JOBS = [
        self::JOB_ADMIN   => 'ادمین',
        self::JOB_GENERAL => 'کاربر عمومی',
        self::JOB_AUTHOR  => 'نویسنده',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_MOBILE,
        self::COLUMN_EMAIL,
        self::COLUMN_FIRST_NAME,
        self::COLUMN_LAST_NAME,
        self::COLUMN_PASSWORD,
        self::COLUMN_MOBILE,
        self::COLUMN_EMAIL,
        self::COLUMN_JOB,
        self::COLUMN_EMAIL_VERIFIED_AT,
        self::COLUMN_MOBILE_VERIFIED_AT,
    ];

    public function setPassword(int $value): self
    {
        $this->{self::COLUMN_PASSWORD} = Hash::make($value);
        return $this;
    }

    public function getMobile(): ?string
    {
        return $this->{self::COLUMN_MOBILE};
    }
    public function setMobile(?string $value): self
    {
        $this->{self::COLUMN_MOBILE} = $value;
        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->{self::COLUMN_EMAIL};
    }
    public function setEmail(?string $value): self
    {
        $this->{self::COLUMN_EMAIL} = $value;
        return $this;
    }

    public function getFirstName(): ?string
    {
        return $this->{self::COLUMN_FIRST_NAME};
    }
    public function setFirstName(?string $value): self
    {
        $this->{self::COLUMN_FIRST_NAME} = $value;
        return $this;
    }
    public function getLastName(): ?string
    {
        return $this->{self::COLUMN_LAST_NAME};
    }
    public function setLastName(?string $value): self
    {
        $this->{self::COLUMN_LAST_NAME} = $value;
        return $this;
    }

    public function getJob(): ?string
    {
        return $this->{self::COLUMN_JOB};
    }
    public function setJob(?string $value): self
    {
        $this->{self::COLUMN_JOB} = $value;
        return $this;
    }

    public function getFullName(): ?string
    {
        if (!$this->getFirstName() && !$this->getLastName()) {
            return null;
        }
        return $this->getFirstName().' '.$this->getLastName();
    }

    public static function getById(int $id): self
    {
        return User::where(User::COLUMN_ID,'=',$id)->first();
    }

    public static function getAll(): Collection
    {
        return User::where(User::COLUMN_ACTIVE,'<>',null)
            ->orderBy(User::COLUMN_ID)
            ->get();
    }

    public static function getByEmail($email): ?self
    {
        return User::where(User::COLUMN_EMAIL,$email)->first();
    }

    public function save(array $options = []): bool
    {
        $result = parent::save($options);

        if ($result && empty($options)) {
            //UserSapIntegrationJob::dispatch($this);
        }

        return $result;
    }
}
