<?php
namespace App\Models\Basic;

use Illuminate\Support\Facades\Auth;
//use App\Jobs\Basic\UserSapIntegrationJob;
//use App\Notifications\PasswordResetNotification;
//use App\Notifications\sendVerificationNotification;
use Hash;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\Models\BaseModel;

/**
 * @mixin Builder;
 * @mixin QueryBuilder;
 */
class UserProfile extends BaseModel// implements MustVerifyEmail
{
    public static function getDBTable(): string
    {
        return 'user_profiles';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_ID = 'id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_PERSONAL_ID = 'personal_id';
    const COLUMN_PRE_HISTORY = 'pre_history';
    const COLUMN_CUR_HISTORY = 'cur_history';
    const COLUMN_SOCIAL_LINKS = 'social_links';
    const COLUMN_ACTIVITY_RANGE = 'activity_range';
    const COLUMN_JOB = 'job';
    const COLUMN_ACTIVE = 'active';

    const ACTIVITY_RANGE_SALE = 'sale';
    const ACTIVITY_RANGE_RENT = 'rent';
    const ACTIVITY_GENERAL = 'general';

    const ACTIVITY_RANGE = [
        self::ACTIVITY_RANGE_SALE => 'فروش',
        self::ACTIVITY_RANGE_RENT => 'اجاره',
        self::ACTIVITY_GENERAL        => 'عمومی',
    ];

    const JOB_SUPER_USER  = 'super_user';
    const JOB_RANGE_MANAGER  = 'range_manager';
    const JOB_OFFICE_MANAGER  = 'office_manager';
    const JOB_CONSULTANT        = 'consultant';
    const JOB_GENERAL                = 'general';

    const JOBS = [
        self::JOB_SUPER_USER  => 'مدیر ارشد',
        self::JOB_RANGE_MANAGER  => 'مدیر رنج',
        self::JOB_OFFICE_MANAGER  => 'مدیر دفتر',
        self::JOB_CONSULTANT        => 'مشاور',
        self::JOB_GENERAL                => 'عمومی',
    ];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        self::COLUMN_USER_ID,
        self::COLUMN_PERSONAL_ID,
        self::COLUMN_PRE_HISTORY,
        self::COLUMN_CUR_HISTORY,
        self::COLUMN_SOCIAL_LINKS,
        self::COLUMN_ACTIVITY_RANGE,
        self::COLUMN_JOB,
        self::COLUMN_ACTIVE
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
       // self::COLUMN_PASSWORD, self::COLUMN_REMEMBER_TOKEN,
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
       // self::COLUMN_EMAIL_VERIFIED_AT => 'datetime',
    ];

    public function getUserId(): ?string
    {
        return $this->{self::COLUMN_USER_ID};
    }
    public function setUserId(?string $value): self
    {
        $this->{self::COLUMN_USER_ID} = $value;
        return $this;
    }

    public function getPersonalId(): ?int
    {
        return $this->{self::COLUMN_PERSONAL_ID};
    }
    public function setPersonalId(?int $value): self
    {
        $this->{self::COLUMN_PERSONAL_ID} = $value;
        return $this;
    }

    public function getPreHistory(): ?string
    {
        return $this->{self::COLUMN_PRE_HISTORY};
    }
    public function setPreHistory(?string $value): self
    {
        $this->{self::COLUMN_PRE_HISTORY} = $value;
        return $this;
    }

    public function getCurHistory(): ?string
    {
        return $this->{self::COLUMN_CUR_HISTORY};
    }
    public function setCurHistory(?string $value): self
    {
        $this->{self::COLUMN_CUR_HISTORY} = $value;
        return $this;
    }

    public function getSocialLinks(): ?string
    {
        return $this->{self::COLUMN_SOCIAL_LINKS};
    }
    public function setSocialLinks(?string $value): self
    {
        $this->{self::COLUMN_SOCIAL_LINKS} = $value;
        return $this;
    }

    public function getActivityRang(): ?string
    {
        return $this->{self::COLUMN_ACTIVITY_RANGE};
    }
    public function setActivityRang(?string $value): self
    {
        $this->{self::COLUMN_ACTIVITY_RANGE} = $value;
        return $this;
    }

    public function getJob(): ?string
    {
        return $this->{self::COLUMN_JOB};
    }
    public function setJob(?string $value): self
    {
        $this->{self::COLUMN_JOB} = $value;
        return $this;
    }

    public static function getById(int $id): ?self
    {
        return (new UserProfile())
            ->where(UserProfile::COLUMN_USER_ID,'=',$id)
            ->first();
    }

    public static function getByUserId(int $Userid)
    {
        return (new UserProfile())
            ->where(UserProfile::COLUMN_USER_ID,'=',$Userid)
            ->first();
    }

    public function save(array $options = []): bool
    {
        $result = parent::save($options);

        if ($result && empty($options)) {
            //UserSapIntegrationJob::dispatch($this);
        }

        return $result;
    }
}
