<?php
namespace App\Models\Basic;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Query\Builder as QueryBuilder;
use App\Models\BaseModel;

/**
 * @mixin Builder;
 * @mixin QueryBuilder;
 */
class UserImages extends BaseModel// implements MustVerifyEmail
{
    public static function getDBTable(): string
    {
        return 'user_images';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_ID = 'id';
    const COLUMN_USER_ID = 'user_id';
    const COLUMN_IMAGE_FILE_PATH = 'image_file_path';
    const COLUMN_ACTIVE = 'active';

    const BUY_REQUIRED_FIELDS = [
        self::COLUMN_IMAGE_FILE_PATH,
    ];

    public function getUserId(): ?string
    {
        return $this->{self::COLUMN_USER_ID};
    }
    public function setUserId(?string $value): self
    {
        $this->{self::COLUMN_USER_ID} = $value;
        return $this;
    }

    public function getImageFilePath(): ?string
    {
        return $this->{self::COLUMN_IMAGE_FILE_PATH};
    }
    public function setImageFilePath(?string $value): self
    {
        $this->{self::COLUMN_IMAGE_FILE_PATH} = $value;
        return $this;
    }

    public function getIsActive(): bool
    {
        return $this->{self::COLUMN_ACTIVE};
    }

    public function setIsActive(bool $value): self
    {
        $this->{self::COLUMN_ACTIVE} = $value;
        return $this;
    }

    public static function getById(int $id): self
    {
        return (new UserImages())
            ->where(UserImages::COLUMN_ID,'=',$id)
            ->first();
    }

    public static function getByUserId(?int $userId): ?self
    {
        return (new UserImages())
            ->where(UserImages::COLUMN_USER_ID,'=',$userId)
            ->first();
    }

    public function save(array $options = []): bool
    {
        $result = parent::save($options);

        if ($result && empty($options)) {
            //UserSapIntegrationJob::dispatch($this);
        }

        return $result;
    }
}
