<?php
namespace App\Models\Basic;

use App\Models\BaseModel;
use Illuminate\Pagination\Paginator;
use Illuminate\Support\Collection;


class UserViewed extends BaseModel
{
    public static function getDBTable(): string
    {
        return 'user_viewed';
    }
    public static function getGroup(): string
    {
        return 'Basic';
    }

    const COLUMN_USER_ID = 'user_id';
    const COLUMN_ADV_ID = 'adv_id';
    const COLUMN_USER_IP = 'user_ip';

    public function getUserId(): ?string
    {
        return $this->{self::COLUMN_USER_ID};
    }
    public function setUserId(?string $value): self
    {
        $this->{self::COLUMN_USER_ID} = $value;
        return $this;
    }

    public function getAdvId(): ?int
    {
        return $this->{self::COLUMN_ADV_ID};
    }
    public function setAdvId(?int $value): self
    {
        $this->{self::COLUMN_ADV_ID} = $value;
        return $this;
    }

    public function getUserIp(): ?string
    {
        return $this->{self::COLUMN_USER_IP};
    }
    public function setUserIp(?string $value): self
    {
        $this->{self::COLUMN_USER_IP} = $value;
        return $this;
    }

    public static function getById(int $id): self
    {
        return (new UserViewed())
            ->where(UserViewed::COLUMN_ID,'=',$id)
            ->first();
    }

    public static function getByUserId(int $Userid): self
    {
        return (new UserViewed())
            ->where(UserViewed::COLUMN_USER_ID,'=',$Userid)
            ->first();
    }

    public static function getAllByUserId(int $Userid)
    {
        return (new UserViewed())
            ->where(UserViewed::COLUMN_USER_ID,'=',$Userid)
            ->orderBy(UserViewed::COLUMN_ID)
            ->paginate(10);
    }

    public static function getExistViewedByUserIdAdvId(?int $advId,?int $userId): bool
    {
        return (new UserViewed())
            ->where(UserViewed::COLUMN_ADV_ID,'=',$advId)
            ->where(UserViewed::COLUMN_USER_ID,'=',$userId)
            ->exists();
    }

    public function save(array $options = []): bool
    {
        $result = parent::save($options);

        if ($result && empty($options)) {
            //UserSapIntegrationJob::dispatch($this);
        }

        return $result;
    }
}
